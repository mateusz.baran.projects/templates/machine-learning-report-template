Machine Learning report template 
==============================

An example of the Machine Learning report template based on [NeurIPS 2020 Style Files](https://nips.cc/Conferences/2020/PaperInformation/StyleFiles).

Instructions
------------
1. Clone the repo.
1. Run `rm -rf .git && rm README.md` to remove `.git` directory and `README.md` file.
 
Report Structure
------------

    ├── figures
    │   ├── sec1
    │   │   ├── fig-`name`.png
    │   │   └── fig-`name`.tex
    │   │
    │   └── sec2
    │       ├── fig-`name`.png
    │       └── fig-`name`.tex
    │
    ├── tables
    │   ├── sec1 
    │   │   └── tab-`name`.tex
    │   │
    │   └── sec2 
    │       └── tab-`name`.tex
    │
    ├── sections
    │   ├── abstract.tex
    │   ├── sec1-`name`.tex
    │   ├── sec1sub1-`name`.tex
    │   ├── sec1sub2-`name`.tex
    │   ├── sec2-`name`.tex
    │   ├── sec2sub1-`name`.tex
    │   ├── sec2sub2-`name`.tex
    │   └── summary.tex
    │
    ├── appendices
    │   ├── secA-`name`.tex
    │   └── secB-`name`.tex
    │
    ├── LICENSE
    ├── README.md
    ├── main.tex
    ├── neurips.sty
    ├── preamble.sty
    └── references.bib
    